from weasyprint import HTML, CSS
from jinja2 import Template
from datetime import datetime


def content2list(txtfile):
    with open(txtfile) as _content:
        content = _content.read()
        content_list = content.split('\n')
    return content_list

with open('email_template.html') as _html:
    email_template = Template(_html.read())

# can have the css stylesheet as a template
# so that the subject can go on to the header of the page
with open('style_template.css') as _css:
    css_template = Template(_css.read())


email_header_dict ={"from": "hellokitty@mintlab.nl",
                    "to": "batman@mintlab.nl",
                    "cc": ["robin@mintlab.nl",
                           "spiderman@mintlab.nl",
                           "wonderwoman@mintlab.nl"],
                    "date": datetime.now().isoformat(),
                    "subject": "Hello PDF"}

# render templates
email_html = email_template.render(
    subject=email_header_dict.get('subject'),
    emailheader=email_header_dict,
    content_list=content2list('content.txt')
)

email_css = css_template.render(subject=email_header_dict.get('subject'))

# give HTML and CSS to Weasyprint to create PDF
html = HTML(string=email_html)
css = CSS(string=email_css)
html.write_pdf('example.pdf', stylesheets=[css])

